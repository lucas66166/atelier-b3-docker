# GESTION PRODUITS

## Prérequis
Cette application est compatible `PHP8.3` et a été testée avec une base de données `MySQL 8.3`.

## Installation
- Assurez vous d'avoir Docker d'installé sur votre machine
- A la racine du projet, exécutez `docker compose up --build`
- Rendez vous sur http://localhost:8080
- Connectez vous à l'application avec l'url adaptée avec les informations suivantes :
    - Login : `admin`
    - Mot de passe : `password`

## Fonctionnalités
L'application permet de :
- Lister les produits
- Afficher la fiche produit en lecture seule
- Ajouter des produits
- Modifier les produits
- Supprimer les produits
- Pour chaque produit, il est possible d'ajouter autant de photos que nécessaire
